export const checkInputs = ({ params = null} = {}) => {

    if(!process.env.GITLAB_ACCESS_KEY || typeof process.env.GITLAB_ACCESS_KEY === 'undefined' || process.env.GITLAB_ACCESS_KEY === ""){
        throw new Error("No access key defined");
    }

    if(!params || typeof params === 'undefined' || params === ""){
        throw new Error("No params provided");
    }

    if(!params.milestone){
        throw new Error("No milestone provided");
    }

    return true;
};
