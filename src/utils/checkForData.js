export const checkForData = ({ issues = []} = {}) => {

    if(issues.length === 0){
        return [];
    }

    if(Array.isArray(issues)){
        return issues;
    }

    if(Array.isArray(issues.data)){
        return issues.data;
    }

    return issues;

};
