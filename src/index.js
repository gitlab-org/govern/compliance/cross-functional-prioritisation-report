import express, { json } from 'express';
import * as dotenv from 'dotenv'
dotenv.config()
import { checkInputs, checkForData } from './utils/index.js';
import { config, output, missedMilestoneLabels, milestoneClosedLabels } from './config/index.js';
import { createReport } from './report/index.js'
import { getIssues, countIssueWeight, countIssueWeightLabel, countIssuesLabel, issueUrls, issueUrlsLabel } from './issues/index.js';
import { createComment } from './comment/index.js';

const app = express();
const PORT = process.env.PORT || 3001;

app.get("/", async (request, response, next) => {

    try {
        const params =  request?.query;
        checkInputs({ params });

        // Milestone
        const milestone = params.milestone;
        output.milestone = milestone;

        //Milestone Closed Issues
        const milestoneClosedIssuesReturn = await getIssues({ 
            milestone, 
            queryParams: { 
                labels: `group::${config.group}`,
                state: "closed" 
            } 
        });
        const milestoneClosedIssues = checkForData({ issues: milestoneClosedIssuesReturn })

        output.closed.total = {
            count: milestoneClosedIssues.length,
            weight: countIssueWeight({ issues: milestoneClosedIssues }),
            issues: issueUrls({ issues: milestoneClosedIssues })
        }

        milestoneClosedLabels.forEach(milestoneClosedLabel => {
            const countIssues = countIssuesLabel({ 
                issues: milestoneClosedIssues,
                label: milestoneClosedLabel.label,
            });

            output.closed[milestoneClosedLabel.var] = {
                weight: countIssueWeightLabel({ 
                    issues: milestoneClosedIssues,
                    label: milestoneClosedLabel.label,
                }),
                count: countIssues,
                issues: issueUrlsLabel({ 
                    issues: milestoneClosedIssues,
                    label: milestoneClosedLabel.label,
                })
            }
        });

        //Milestone Missed Issues
        const missedMilestoneIssuesReturn = await getIssues({ 
            queryParams: {
                labels: `group::${config.group},missed:${milestone}`
            } 
        });
        const missedMilestoneIssues = checkForData({ issues: missedMilestoneIssuesReturn })

        output.missed.total = {
            count: missedMilestoneIssues.length,
            weight: countIssueWeight({ issues: missedMilestoneIssues }),
            issues: issueUrls({ issues: missedMilestoneIssues })
        }

        missedMilestoneLabels.forEach(missedMilestoneLabel => {

            const countIssues = countIssuesLabel({ 
                issues: missedMilestoneIssues,
                label: missedMilestoneLabel.label,
            });

            output.missed[missedMilestoneLabel.var] = {
                weight: countIssueWeightLabel({ 
                    issues: missedMilestoneIssues,
                    label: missedMilestoneLabel.label,
                }),
                count: countIssues,
                issues: issueUrlsLabel({ 
                    issues: missedMilestoneIssues,
                    label: missedMilestoneLabel.label,
                })
            }
        });


        // Create the report comment
        const report = createReport({ data: output });

        const commentCreated = await createComment({ 
            project_id: config.gitlab_project_id,
            issue_id: config.gitlab_issue_id,
            body: report
        });

        response.json({ 
            output,
            report,
            commentCreated
        });
    } catch(error) {
        console.log({error})
        next(error);
    }
});

app.use((error, request, response, next) => {
    response.status(500).send(`Error: ${error}`);
})

app.listen(PORT, () => console.log(`App listening at port ${PORT}`));