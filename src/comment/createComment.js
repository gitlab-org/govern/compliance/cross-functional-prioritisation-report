import { gitlabApi } from "../hooks/index.js";

export const createComment = async ({ project_id = null, issue_id = null, body = null} = {}) => {

    const options = {
        projectId: project_id,
        issueIId: issue_id,
        body,
    };

    try {
        // https://docs.gitlab.com/ee/api/notes.html#create-new-issue-note
        const issue = await gitlabApi.IssueNotes.create( 
            project_id,
            issue_id,
            body,
        );

        return issue;
    } catch (err) {
        const error = `createComment: ${err.message}`;

        throw new Error(error);
    }
};
