export const config = {
    gitlab_project_id: '',
    gitlab_issue_id: '', 
    group: '',
};

export const output = {
    milestone: '',
    closed: {
        total: {},
        frontend: {},
        backend: {},
        feature: {},
        maintenance: {},
        bug: {},
    },
    missed: {
        total: {},
        frontend: {},
        backend: {},
        feature: {},
        maintenance: {},
        bug: {}, 
        design: {},            
        solutionValidation: {},            
        planningBreakdown: {},            
        readyForDevelopment: {},            
        blocked: {},            
        inDev: {},            
        inReview: {},            
        awaitingSecurityRelease: {},            
        verification: {}, 
    },
};

export const missedMilestoneLabels = [
    {
        var: 'frontend',
        label: 'frontend', 
    },
    {
        var: 'backend',
        label: 'backend', 
    },
    {
        var: 'feature',
        label: 'type::feature', 
    },
    {
        var: 'maintenance',
        label: 'type::maintenance', 
    },
    {
        var: 'bug',
        label: 'type::bug', 
    },
    {
        var: 'design',
        label: 'workflow::design', 
    },
    {
        var: 'solutionValidation',
        label: 'workflow::solution validation', 
    },
    {
        var: 'planningBreakdown',
        label: 'workflow::planning breakdown', 
    },
    {
        var: 'readyForDevelopment',
        label: 'workflow::ready for development', 
    },
    {
        var: 'inDev',
        label: 'workflow::in dev', 
    },
    {
        var: 'inReview',
        label: 'workflow::in review', 
    },
    {
        var: 'awaitingSecurityRelease',
        label: 'workflow::awaiting security release', 
    },
    {
        var: 'verification',
        label: 'workflow::verification', 
    },
];

export const milestoneClosedLabels = [
    {
        var: 'frontend',
        label: 'frontend', 
    },
    {
        var: 'backend',
        label: 'backend', 
    },
    {
        var: 'feature',
        label: 'type::feature', 
    },
    {
        var: 'maintenance',
        label: 'type::maintenance', 
    },
    {
        var: 'bug',
        label: 'type::bug', 
    },
];