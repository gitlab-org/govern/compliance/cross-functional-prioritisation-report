export const countIssueWeight = ({ issues = [] } = {}) => {

    if(issues.length === 0){
        return 0;
    }

    const totalValue = issues.reduce((sum, issue) => {
        if (issue.weight === null) {
            return sum;
        }
        return sum + issue.weight;
    }, 0);

    return totalValue;

};
