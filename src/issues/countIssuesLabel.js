export const countIssuesLabel = ({ issues = [], label = null} = {}) => {

    if(issues.length === 0 || !label){
        return 0;
    }

    const filteredIssues = issues.filter(issue => {
        return issue.labels.includes(label);
    });

    const totalValue = filteredIssues.length;

    return totalValue;

};