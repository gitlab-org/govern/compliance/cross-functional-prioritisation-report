import { countIssueWeight } from './index.js';

export const countIssueWeightLabel = ({ issues = [], label = null} = {}) => {

    if(issues.length === 0 || !label){
        return 0;
    }

    const filteredIssues = issues.filter(issue => {
        return issue.labels.includes(label);
      });

    const totalValue = countIssueWeight({ issues: filteredIssues });

    return totalValue;

};
