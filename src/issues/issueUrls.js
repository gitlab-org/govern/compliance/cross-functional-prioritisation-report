export const issueUrls = ({ issues = [] } = {}) => {

    if(issues.length === 0){
        return [];
    }

    const issueUrls = issues.map((issue) => {
        return issue.web_url;
    });

    return issueUrls;

};
