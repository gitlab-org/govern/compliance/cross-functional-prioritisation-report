import { gitlabApi } from "../hooks/index.js";

export const getIssues = async ({ milestone = null, queryParams = {}} = {}) => {

    try {

        // https://gitlab.com/api/v4/issues?labels=group::compliance&milestone=15.7&scope=all
        // https://docs.gitlab.com/ee/api/issues.html
        const issueBasicParams = { 
            milestone: milestone, 
            scope: "all",
            showExpanded: true 
        };
        const issueParams = { ...issueBasicParams, ...queryParams };
        const issues = await gitlabApi.Issues.all(issueParams);

        return issues;
    } catch (err) {
        const error = `getIssues: ${err.message}`;

        throw new Error(error);
    }
};
