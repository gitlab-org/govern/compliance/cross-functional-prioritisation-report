import { issueUrls } from './index.js';

export const issueUrlsLabel = ({ issues = [], label = null} = {}) => {

    if(issues.length === 0 || !label){
        return [];
    }

    const filteredIssues = issues.filter(issue => {
        return issue.labels.includes(label);
    });

    const totalValue = issueUrls({ issues: filteredIssues });

    return totalValue;

};
