# Cross Functional Prioritisation Report

This project is to generate a report of issue weight planned vs delivered within the Compliance team.

The report is uploaded as a comment on a GitLab Project Issue

## Getting started

Clone down the repo

Run `npm install` to install all dependencies

Next you will need to update the config and add an access token

1. Copy `.env.example` over to `.env`
1. Update the variable `GITLAB_ACCESS_KEY` with a personal access token. How to get one https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
1. Open file `./config/index.js`
1. Update the `config` object at the top with your options. You will need to create an issue in your project on Gitlab first https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#from-a-project

Run `npm run start` to start the project

Open your browser to http://localhots:3001/?milestone=XX.X updating the milestone on the end

THis will automatically create a note in your specified Issue
